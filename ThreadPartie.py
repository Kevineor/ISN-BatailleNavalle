import threading, socket, random
from Bateau import *
from pygame import time

class ThreadPartie(threading.Thread):

    def __init__(self, joueur1, joueur2):
        super().__init__()
        self.joueurs = [joueur1, joueur2]
        for joueur in self.joueurs:
            joueur.send("PartieTrouvee".encode())
        self.joueurPret = [False, False]
        self.commencer = False
        self.listeBateaux = [[], []]
        self.continuer = True
        self.clock = time.Clock()


    def run(self):
        nomPartie = self.getName()
        print("Partie {} Démarée".format(nomPartie))
        #crée un booleen choissisant le premier joueur
        joueurTour = random.random() < 0.5
        joueurActuel = False
        while self.continuer:
            self.clock.tick(30)
            self.joueurs[joueurActuel].setblocking(0)
            try:
                messageRecu = self.joueurs[joueurActuel].recv(1024).decode().split(".")
            except socket.error:
                self.joueurs[joueurActuel].setblocking(1)
                pass
            else:
                self.joueurs[joueurActuel].setblocking(1)
                for message in messageRecu:
                    print(message)
                    messageSplit = message.split(",")
                    if messageSplit[0] == "fin":
                        for joueur in self.joueurs:
                            joueur.send("fin.".encode())
                        self.continuer = False
                    elif messageSplit[0] == "Bateau":
                        self.listeBateaux[joueurActuel].append(Bateau.CreerSansImage(message))
                    elif messageSplit[0] == "Tir" and self.commencer and joueurTour == joueurActuel:
                        #On envoi le tir au joueur enemi, a lui de l'interpreter
                        self.joueurs[not joueurActuel].send((message + ".").encode())
                        bateauTouche = pygame.Rect((int(messageSplit[1]) * 32, int(messageSplit[2]) * 32), (1, 1)).collidelist(self.listeBateaux[not joueurActuel])
                        if bateauTouche > -1:
                            self.listeBateaux[not joueurActuel][bateauTouche].detruireCase(int(messageSplit[1]), int(messageSplit[2]))
                            #on indique au joueur qui a joue, Q'il a touche un bateau
                            self.evoyerTouche(self.joueurs[joueurActuel], int(messageSplit[1]), int(messageSplit[2]))
                            if self.listeBateaux[not joueurActuel][bateauTouche].detruit():
                                self.envoyerCoule(self.joueurs[joueurActuel], self.listeBateaux[not joueurActuel][bateauTouche])
                            if self.perdu(not joueurActuel):
                                self.envoyerGagne(self.joueurs[joueurActuel])
                                self.envoyerPerdu(self.joueurs[not joueurActuel])
                                self.continuer = False
                        else:
                            # on indique au joueur qui a joue, Q'il a Rate les bateaux
                            self.evoyerRate(self.joueurs[joueurActuel], int(messageSplit[1]), int(messageSplit[2]))
                        # on passe le tour a l'autre joueur
                        joueurTour = not joueurTour
                        self.envoyerTour(self.joueurs[joueurTour])
                    elif messageSplit[0] == "Pret":
                        self.joueurPret[joueurActuel] = True
                        self.commencer = True
                        for joueur in self.joueurPret:
                            if joueur == False:
                                self.commencer = False
                        if self.commencer:
                            self.envoyerTour(self.joueurs[joueurTour])
            joueurActuel = not joueurActuel
        # Apres le break on ferme les socket
        for joueur in self.joueurs:
            joueur.close()
        print("Partie {} Terminée".format(nomPartie))


    def envoyerGagne(self, joueur):
        joueur.setblocking(1)
        joueur.send("Gagne.".encode())
        joueur.setblocking(0)

    def envoyerPerdu(self, joueur):
        joueur.setblocking(1)
        joueur.send("Perdu.".encode())
        joueur.setblocking(0)

    def envoyerCoule(self, joueur, bateau):
        joueur.setblocking(1)
        joueur.send(str(bateau).encode())
        joueur.setblocking(0)

    def evoyerTouche(self, joueur, positionX, positionY):
        joueur.setblocking(1)
        joueur.send("Touche,{},{}.".format(positionX, positionY).encode())
        joueur.setblocking(0)

    def evoyerRate(self, joueur, positionX, positionY):
        joueur.setblocking(1)
        joueur.send("Rate,{},{}.".format(positionX, positionY).encode())
        joueur.setblocking(0)

    def envoyerTour(self, joueur):
        joueur.setblocking(1)
        joueur.send("Tour.".encode())
        joueur.setblocking(0)

    def perdu(self, njoueur):
        perdu = True
        for bateau in self.listeBateaux[njoueur]:
            if bateau.detruit() == False:
                perdu = False
        return perdu

