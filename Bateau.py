import pygame
from ObjetAffichable import ObjetAffichable
import sys


class Bateau(ObjetAffichable):

    def __init__(self, positionX, positionY, type, initialiserImage = True):
        # si orientation = 0 -> horizontal
        # si orientation = 1 -> vertical
        self.orientation = 0
        #on vérifie si la position est définie
        self.type = type
        # torpilleur
        if type == 0:
            self.longueur = 2
            self.hauteur = 1
            self.cheminImage = "assets/bateau/torpilleur.png"
        # sous-marin
        elif type == 1:
            self.longueur = 3
            self.hauteur = 1
            self.cheminImage = "assets/bateau/sous-marin.png"
        # contre-torpilleur
        elif type == 2:
            self.longueur = 3
            self.hauteur = 1
            self.cheminImage = "assets/bateau/contre-torpilleur.png"
        # croiseur
        elif type == 3:
            self.longueur = 4
            self.hauteur = 1
            self.cheminImage = "assets/bateau/croiseur.png"
        # porte-avion
        elif type == 4:
            self.longueur = 5
            self.hauteur = 1
            self.cheminImage = "assets/bateau/porte-avion.png"
        else:
            raise ValueError("type de bateau inconnu")

        self.casesDetruites = self.creerTableauCasesDetruites()
        self.initialiserImage = initialiserImage
        if self.initialiserImage:
            self.imageCaseDetruite = pygame.transform.scale(pygame.image.load("assets/Jeu/Touche.png").convert_alpha(), (32, 32))
            #on applique un effet de transparence de l'image detruite
            self.rendreTransparent(self.imageCaseDetruite)


        super().__init__(positionX, positionY, self.longueur, self.hauteur, self.cheminImage, self.initialiserImage)

    #Place le Bateau Horisontal a vertical et inversement
    def tourner(self):
        #A FAIRE (Inverser hauteur et largeur + verifier position + actualiser tableau casesDetruites
        self.longueur, self.hauteur = self.hauteur, self.longueur
        if self.orientation == 0:
            if self.initialiserImage:
                self.image = pygame.transform.rotate(self.image, -90)
            self.orientation = 1
        elif self.orientation == 1:
            if self.initialiserImage:
                self.image = pygame.transform.rotate(self.image, 90)
            self.orientation = 0
        self.casesDetruites = self.creerTableauCasesDetruites()



    def afficher(self, ecran):
        if self.initialiserImage:
            super().afficher(ecran)
            #on ajoute au bateau, l'affichage des cases detruites
            for x in range(self.longueur):
                for y in range(self.hauteur):
                    if self.casesDetruites[x][y]:
                        ecran.blit(self.imageCaseDetruite, ((self.positionX + x) * 32, (self.positionY + y) * 32))

    #Methode permettant de détruire la case du tableau (on considère la case sur le bateau)
    def detruireCase(self, x, y):
        #x et y position absolue sur l'ecran on enlève la position du bateau et on divise par 32 on connait la position de la case detruite
        x -= int(self.positionX)
        y -= int(self.positionY)
        self.casesDetruites[x][y] = True


    #Methode renvoyant True si le bateau est completement detruit et False sinon
    def detruit(self):
        detruit = True
        for liste in self.casesDetruites:
            for case in liste:
                if case == False:
                    detruit = False
        return detruit


    #Methode détruisant entièrement un bateau
    def detruire(self):
        for x in range(len(self.casesDetruites)):
            for y in range(len(self.casesDetruites[x])):
                self.casesDetruites[x][y] = True

    #Renvoie la chaine de caractère correspondant au bateau
    def __str__(self):
        return "Bateau,{},{},{},{}.".format(int(self.positionX), int(self.positionY), int(self.type), int(self.orientation))


    #creation d'un bateau a partir d'une chaine de caractere pour le reseau
    @staticmethod
    def Creer(str):
        donneesBateau = str.split(',')
        if donneesBateau[0] != "Bateau":
            sys.exit()
        bateau = Bateau(int(donneesBateau[1]), int(donneesBateau[2]), int(donneesBateau[3]))
        if int(donneesBateau[4]) == 1:
            bateau.tourner()
        print("Creation d'un bateau a partir d'un str : {}".format(bateau))
        return bateau

    #creation d'un bateau a partir d'une chaine de caractere pour le reseau (sans initialiser l'image)
    @staticmethod
    def CreerSansImage(str):
        donneesBateau = str.split(',')
        if donneesBateau[0] != "Bateau":
            sys.exit()
        bateau = Bateau(int(donneesBateau[1]), int(donneesBateau[2]), int(donneesBateau[3]), False)
        if int(donneesBateau[4]) == 1:
            bateau.tourner()
        print("Creation d'un bateau a partir d'un str : {}".format(bateau))
        return bateau

    #Methode créant un tableau de cases detruites
    def creerTableauCasesDetruites(self):
        casesDetruites = [[False] * self.hauteur for _ in range(self.longueur)]
        return casesDetruites

