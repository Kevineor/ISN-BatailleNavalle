import socket, select
from ThreadPartie import *



port = 50000

connexion = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
connexion.bind(('', port))

connexion.listen(5)

print("Le serveur écoute à présent sur le port {}".format(port))

clients = []
clients_en_jeu = []
Parties = []

while 1:
    connexions_demandees, wlist, xlist = select.select([connexion], [], [], 0.05)
    for connexion in connexions_demandees:
        connexion_avec_client, infos_connexion = connexion.accept()
        clients.append(connexion_avec_client)
        connexion_avec_client.send("En attente.".encode())

    #On check les clients a fermer coté clients en attente
    clients_a_fermer = []
    try:
        clients_a_fermer, wlist, xlist = select.select(clients, [], [], 0.05)
    except select.error:
        pass
    else:
        for client in clients_a_fermer:
            msg_recu = client.recv(1024).decode().split(".")
            for message in msg_recu:
                print("Reçu {}".format(message))
                if message == "fin":
                    client.send("fin.".encode())
                    client.close()
                    clients.remove(client)

    while len(clients) >= 2:
        partie = ThreadPartie(clients[0], clients[1])
        partie.start()
        clients_en_jeu.append(clients.pop(1))
        clients_en_jeu.append(clients.pop(0))

    # client, adresse = connexion.accept()
    # client.send("En attente".encode())
    # client2, adresse2 = connexion.accept()
    # client.send("Joueur trouve".encode())
    # thread = ThreadPartie(client, client2)
    # thread.run()
    # Parties.append(thread)