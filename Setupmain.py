from cx_Freeze import setup, Executable

options = {
    'build_exe': {
        'include_msvcr': True,
        'include_files': [
            "assets",
            "ListeIp.txt"
        ]
    }
}

setup (
    name = "BattleChips",
    version = "2",
    description = "Jeu bataille navale ISN ANDRIEUX DEGIOANNI MASSON",
    options = options,
    executables = [Executable("main.py", base="Win32GUI", targetName="ISN-BatailleNavale.exe")],
)