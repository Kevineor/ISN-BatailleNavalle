from ObjetAffichable import *
import pygame


class Bouton(ObjetAffichable):
    def __init__(self, positionX, positionY, longueur, hauteur, cheminImage):
        super().__init__(positionX, positionY, longueur, hauteur, cheminImage)
        self.surligne = False
        self.rectSurligne = pygame.Rect(self.x, self.y, self.w, self.h)
        centre = self.rectSurligne.center
        self.rectSurligne.w += 6
        self.rectSurligne.h += 6
        self.rectSurligne.center = centre
        self.imageSurligne = pygame.transform.scale(self.image, (self.rectSurligne.w, self.rectSurligne.h))

    def afficher(self, ecran):
        if not self.surligne:
            super().afficher(ecran)
        else:
            ecran.blit(self.imageSurligne, (self.rectSurligne.x, self.rectSurligne.y))

    def pointe(self, event):
        return self.collidepoint(event.positionX, event.positionY)

    def verifSurlignage(self, event):
        if self.pointe(event) and not self.surligne:
            self.surligne = True

        elif not self.pointe(event) and self.surligne:
            self.surligne = False
