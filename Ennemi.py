import pygame
from Bateau import *
import random

class Ennemi:
    def __init__(self, jeu):
        self.listeBateaux = self.creerListeBateauAleatoire()
        self.listeRate = []
        self.listeTouche = []
        self.jeu = jeu
        self.jeu.partieTrouvee = True
        self.ferme = False


    def creerListeBateauxNA(self):
        listeBateauxNA = []
        for i in range(5):
            listeBateauxNA.append(Bateau(0, 0, i, False))
        return listeBateauxNA

    def creerListeBateauAleatoire(self):
        listeBateaux = []
        listeBateauxNA = self.creerListeBateauxNA()

        while len(listeBateauxNA) != 0:

            for bateau in listeBateauxNA:
                orientation = 0
                if random.random() < 0.5:
                    bateau.tourner()
                    orientation = 1
                bateauAPlacer = Bateau(int(random.randint(0, 10) - bateau.longueur), int(random.randint(0, 10) - bateau.hauteur), bateau.type)
                if orientation == 1:
                    bateauAPlacer.tourner()
                if bateauAPlacer.verifPosition() and bateauAPlacer.collidelist(listeBateaux) == -1:
                    listeBateaux.append(bateauAPlacer)
                    listeBateauxNA.remove(bateau)
        return listeBateaux

    def envoyerBateau(self, str):
        #On ignore, l'ennemi n'a pas a recuperer les bateaux
        return

    def jouer(self):
        positionTir = pygame.Rect((-2,-2), (32, 32))

        while positionTir.collidelist(self.listeRate + self.listeTouche) > -1:
            positionTir.x = random.randint(0, 10) * 32
            positionTir.y = random.randint(0, 10) * 32

        touche = self.jeu.verifierTir(positionTir.x // 32, positionTir.y // 32)
        if touche:
            self.listeTouche.append(positionTir)
        else:
            self.listeRate.append(positionTir)
        self.jeu.tour = True

    def envoyerTir(self, positionX, positionY):
        bateauTouche = pygame.Rect((positionX * 32, positionY * 32), (1, 1)).collidelist(self.listeBateaux)
        if bateauTouche > -1:
            #on detruit la case de la partie de l'IA
            self.listeBateaux[bateauTouche].detruireCase(int(positionX), int(positionY))
            #On indique au jeu qu'il a touche
            self.jeu.ajouterToucheEnnemi(positionX, positionY)
            if self.listeBateaux[bateauTouche].detruit():
                self.jeu.ajouterBateauCoule("Bateau,{},{},{},{}".format(int(self.listeBateaux[bateauTouche].positionX), int(self.listeBateaux[bateauTouche].positionY), int(self.listeBateaux[bateauTouche].type), int(self.listeBateaux[bateauTouche].orientation)))
            if self.perdu():
                self.jeu.gagne = True
            if self.jeu.testPerdu():
                self.jeu.perdu = True
        else:
            #On n'a pas besoin de sauvegarder la ou le joueur a rate
            # On indique au jeu qu'il a rate
            self.jeu.ajouterRateEnnemi(positionX, positionY)
        self.jouer()

    def envoyerPret(self):
        if random.random() < 0.5:
            self.jeu.tour = True
        else:
            self.jouer()

    def stop(self):
        self.ferme = True

    def perdu(self):
        perdu = True
        for bateau in self.listeBateaux:
            if bateau.detruit() == False:
                perdu = False
        return perdu