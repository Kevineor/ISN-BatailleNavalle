import pygame
from Ecran import *
from Bateau import *
from Evenement import *
from Ennemi import *
from ThreadSocket import ThreadSocket
import os
from Bouton import Bouton

class Jeu():
    MARGE_COTES = 2 * 32
    MARGE_ENTRE_ENCRAN = 7 * 32

    def __init__(self, ligne, volume):
        self.ecranJoueur = Ecran([])
        self.ecranEnnemi = Ecran([])
        self.ecran = pygame.Surface((self.ecranJoueur.Surface.get_width() + self.ecranEnnemi.Surface.get_width() + self.MARGE_ENTRE_ENCRAN + 2 * self.MARGE_COTES, self.ecranJoueur.Surface.get_height() + self.ecranEnnemi.Surface.get_height() + 2 * self.MARGE_COTES))
        self.fond = pygame.image.load("assets/Jeu/Fond.bmp")
        self.boutons = [pygame.image.load("assets/Jeu/BoutonRouge.png").convert_alpha(), pygame.image.load("assets/Jeu/BoutonVert.png").convert_alpha()]

        self.ligne = ligne
        self.volume = volume

        #Initialisation des listes d'affichage
        self.listeBateaux = []
        self.listeBateauxNA = []
        self.creerListeBateaux()
        self.bateauNASelectionne = -1   # indice du bateau Non Assigné NA actuellement selectionne, -1 si aucune selection
        self.listeBateauxEnnemi = []
        self.listeRateTouche = []
        self.listeRate = []
        self.listeCollisionCadrillage = self.creerListeCollision()

        self.boutonPret = Bouton(14, 7, 3, 1, "assets/Jeu/Pret.png")

        #Etat du jeu
        self.tour = False
        self.pret = False
        self.partieTrouvee = False


        self.event = Evenement()

        if self.ligne:
            with open("ListeIp.txt", "r") as fichierIP:
                listeIp = fichierIP.read()
                listeIp = listeIp.split("\n")
                for ip in listeIp:
                    try:
                        self.enemi = ThreadSocket(self, ip, 50000)
                    except:
                        pass
                    else:
                        self.enemi.start()
                        break
        else:
            self.enemi = Ennemi(self)
        self.gagne = False
        self.perdu = False

    #Crée la liste de bateaux non placés (NA)
    def creerListeBateaux(self):
        self.listeBateauxNA = []
        for i in range(5):
            self.listeBateauxNA.append(Bateau(13, (i * 2) + 2, i))

    #Crée la liste de collisions associé au cadrillage
    def creerListeCollision(self):
        liste = []
        for x in range(10):
            for y in range(10):
                liste.append(pygame.Rect(x * 32, y * 32, 32, 32))
        return liste

    ################################################################
    #           Methode qui met a jour les évènements              #
    ################################################################
    def update_event(self):
        continuer = self.event.update()



        if self.event.clicGauche:
            #Si un bateau NA est selectionné on verifie les collisions avec la grille utilisateur
            if self.bateauNASelectionne > -1:
                indiceBateauAPlacer = pygame.Rect((self.event.positionX - self.MARGE_COTES, self.event.positionY - self.MARGE_COTES), (0,0)).collidelist(self.listeCollisionCadrillage)
                if indiceBateauAPlacer > -1:
                    bateauAPlacer = Bateau(self.listeCollisionCadrillage[indiceBateauAPlacer].x / 32, self.listeCollisionCadrillage[indiceBateauAPlacer].y / 32, self.listeBateauxNA[self.bateauNASelectionne].type)
                    if self.listeBateauxNA[self.bateauNASelectionne].orientation == 1:
                        bateauAPlacer.tourner()
                    if bateauAPlacer.verifPosition() and bateauAPlacer.collidelist(self.listeBateaux) == -1:
                        #Ajout du Bateau à placer
                        self.listeBateaux.append(bateauAPlacer)
                        #Suppression du Bateau NA
                        self.listeBateauxNA.pop(self.bateauNASelectionne)
                        #Déselectionnage du bateau NA
                        self.bateauNASelectionne = -1
            # Si aucun bateau n'est selectionné on verifie si on tente d'en selectionner un
            elif not self.pret:
                for i in range(len(self.listeBateauxNA)):
                    if self.listeBateauxNA[i].collidepoint(self.event.positionX, self.event.positionY):
                        self.bateauNASelectionne = i
                        #On stoppe le clic sinon le clic va produire d'autres choses
                        self.event.clicGauche = False
                bateauARetirer = -1
                for i in range(len(self.listeBateaux)):
                    if self.listeBateaux[i].collidepoint(self.event.positionX - self.MARGE_COTES, self.event.positionY - self.MARGE_COTES):
                        self.listeBateauxNA.append(self.listeBateaux[i])
                        self.bateauNASelectionne = len(self.listeBateauxNA) - 1
                        self.listeBateauxNA[self.bateauNASelectionne].x = self.event.positionX
                        self.listeBateauxNA[self.bateauNASelectionne].y = self.event.positionY
                        bateauARetirer = i
                if bateauARetirer > -1:
                    self.listeBateaux.pop(bateauARetirer)

                if self.partieTrouvee and len(self.listeBateauxNA) == 0 and self.boutonPret.pointe(self.event):
                    for bateau in self.listeBateaux:
                        self.enemi.envoyerBateau(str(bateau))
                    self.pret = True
                    self.enemi.envoyerPret()
            # Si c'est notre tour on va verifier si je tire sur le quadrillage adverse
            if self.tour and self.pret:
                indiceTirAPlacer = pygame.Rect(
                    (self.event.positionX - self.MARGE_COTES - 320 - self.MARGE_ENTRE_ENCRAN,
                     self.event.positionY - self.MARGE_COTES),
                    (0, 0)).collidelist(self.listeCollisionCadrillage)
                # Je regarde si j'ai clique sur le quadrillage
                if indiceTirAPlacer > -1:
                    if self.listeCollisionCadrillage[indiceTirAPlacer].collidelist(self.listeRateTouche) == -1:
                        self.tour = False
                        self.enemi.envoyerTir(self.listeCollisionCadrillage[indiceTirAPlacer].x / 32,
                                              self.listeCollisionCadrillage[indiceTirAPlacer].y / 32)

            self.event.clicGauche = False

        #Si un bateau est selectionné on met a jour sa position pour que il suive la souris
        if self.bateauNASelectionne > -1:
            self.listeBateauxNA[self.bateauNASelectionne].x = self.event.positionX
            self.listeBateauxNA[self.bateauNASelectionne].y = self.event.positionY

            #on verifie qu'on ait pas voulu changer l'orientation du bateau
            if self.event.clicDroit:
                self.listeBateauxNA[self.bateauNASelectionne].tourner()
                self.event.clicDroit = False



#        if self.event.entrer and len(self.listeBateauxNA) == 0 and (not self.pret) and self.partieTrouvee:
#            for bateau in self.listeBateaux:
#                self.enemi.envoyerBateau(str(bateau))
#            self.pret = True
#            self.enemi.envoyerPret()

        self.boutonPret.verifSurlignage(self.event)

        return continuer


    def update(self):
        self.ecran.blit(self.fond, (0, 0))
        if self.partieTrouvee:
            self.ecran.blit(self.boutons[self.tour], (self.ecran.get_width() - 32 - 31, 31))
        self.ecranJoueur.update(self.listeBateaux + self.listeRate)
        self.ecranEnnemi.update(self.listeBateauxEnnemi + self.listeRateTouche)
        self.ecran.blit(self.ecranJoueur.Surface, (self.MARGE_COTES, self.MARGE_COTES))
        self.ecran.blit(self.ecranEnnemi.Surface, (self.MARGE_COTES + self.ecranJoueur.Surface.get_width() + self.MARGE_ENTRE_ENCRAN, self.MARGE_COTES))
        for i in range(len(self.listeBateauxNA)):
            if i != self.bateauNASelectionne:
                self.listeBateauxNA[i].afficher(self.ecran)
        if self.bateauNASelectionne > -1:
            # Creation d'une surface pour pouvoir gerer la transparence du rect vert
            surlignage = pygame.Surface(self.listeBateauxNA[self.bateauNASelectionne].size, pygame.SRCALPHA)
            surlignage.fill((0, 255, 0, 128))
            self.ecran.blit(surlignage, self.listeBateauxNA[self.bateauNASelectionne])
            self.listeBateauxNA[self.bateauNASelectionne].afficher(self.ecran)

        if len(self.listeBateauxNA) == 0 and self.partieTrouvee and (not self.pret):
            self.boutonPret.afficher(self.ecran)

    def verifierTir(self, positionX, positionY):
        touche = False
        bateauTouche = pygame.Rect((positionX * 32, positionY * 32), (1, 1)).collidelist(
            self.listeBateaux)
        if bateauTouche > -1:
            self.listeBateaux[bateauTouche].detruireCase(positionX, positionY)
            touche = True
        else:
            self.ajouterRateJoueur(positionX, positionY)
        return touche

    def testPerdu(self):
        perdu = True
        for bateau in self.listeBateaux:
            if bateau.detruit() == False:
                perdu = False
        return perdu


    def ajouterToucheEnnemi(self, positionX, positionY):
        self.listeRateTouche.append(ObjetAffichable(positionX, positionY, 1, 1, "assets/Jeu/Touche.png"))

    def ajouterRateEnnemi(self, positionX, positionY):
        self.listeRateTouche.append(ObjetAffichable(positionX, positionY, 1, 1, "assets/Jeu/Rate.png"))

    def ajouterRateJoueur(self, positionX, positionY):
        self.listeRate.append(ObjetAffichable(positionX, positionY, 1, 1, "assets/Jeu/Rate.png"))

    def verifierClicBateau(self):
        #A FAIRE
        return

    def stop(self):
        if not self.enemi.ferme:
            self.enemi.stop()

    def ajouterBateauCoule(self, strBateau):
        bateauEnemi = Bateau.Creer(strBateau)
        bateauEnemi.detruire()
        listeIndiceTouche = bateauEnemi.collidelistall(self.listeRateTouche)
        listeIndiceTouche.sort()
        listeIndiceTouche.reverse()
        for indice in listeIndiceTouche:
            self.listeRateTouche.pop(indice)
        self.listeBateauxEnnemi.append(bateauEnemi)