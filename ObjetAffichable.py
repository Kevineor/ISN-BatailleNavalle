import pygame

class ObjetAffichable(pygame.Rect):


    TAILLECASE = 32

    #Getter et Setter de longueur (permet de modifier la taille du Rect associé a l'objet)
    def set_longueur(self, longueur):
        self.__longueur = longueur
        self.width = self.longueur * self.TAILLECASE

    def get_longueur(self):
        return self.__longueur

    longueur = property(get_longueur, set_longueur)

    #Getter et Setter de hauteur (permet de modifier la taille du Rect associé a l'objet + permet de respecter l'encapsulation)
    def set_hauteur(self, hauteur):
        self.__hauteur = hauteur
        self.height = self.hauteur * self.TAILLECASE

    def get_hauteur(self):
        return self.__hauteur

    hauteur = property(get_hauteur, set_hauteur)


    def __init__(self, positionX, positionY, longueur, hauteur, cheminImage, initialiserImage = True):
        self.__longueur = longueur
        self.__hauteur = hauteur
        self.positionX = positionX
        self.positionY = positionY
        super().__init__((self.positionX * self.TAILLECASE, self.positionY * self.TAILLECASE, self.longueur * self.TAILLECASE, self.hauteur * self.TAILLECASE))

        if initialiserImage:
            self.image = pygame.image.load(cheminImage).convert_alpha()
            self.redimentionner()

    def placer(self, positionX, positionY):
        self.positionX = positionX
        self.positionY = positionY


    def redimentionner(self):
        self.image = pygame.transform.scale(self.image, (self.width, self.height))

    def afficher(self, ecran):
        ecran.blit(self.image, (self.x, self.y))

    # Methode permettant de rendre transparent une image
    @staticmethod
    def rendreTransparent(surface):
        transparence = pygame.Surface(surface.get_rect().size, pygame.SRCALPHA)
        transparence.fill((255, 255, 255, 140))
        surface.blit(transparence, (0, 0), special_flags=pygame.BLEND_RGBA_MULT)

    #Fonction permettant de verifier que l'objet est bien dans le cadrillage
    def verifPosition(self):
        xOk = False
        if (self.positionX >= 0) and (self.positionX <= 10 - self.longueur):
            xOk = True

        yOk = False
        if (self.positionY >= 0) and (self.positionY <= 10 - self.hauteur):
            yOk = True
        return xOk and yOk
