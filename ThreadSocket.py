import socket
import threading
import sys
from Jeu import *


class ThreadSocket(threading.Thread):

    def __init__(self, jeu, ipserveur, port):
        super().__init__()
        self.jeu = jeu
        self.connexionServeur = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.ferme = False
        self.continuer = True
        print("tentative de connexion avec le serveur...")
        #si la connection n'est pas trouvée avec ce serveur au bout de 3 secondes on debloque le socket (pour en tester un autre)
        self.connexionServeur.settimeout(3)
        try:
            self.connexionServeur.connect((ipserveur, port))
        except socket.error:
            print("La connexion avec le serveur à échoué")
            sys.exit()
        else:
            print("connecté au serveur")
        self.connexionServeur.setblocking(1)

    def run(self):
        while self.continuer:
            messageRecu = self.connexionServeur.recv(1024).decode().split(".")
            for message in messageRecu:
                print(message)
                messageSplit = message.split(",")
                if messageSplit[0] == "fin":
                    # on stoppe le Tread
                    self.continuer = False
                elif messageSplit[0] == "Touche":
                    self.jeu.ajouterToucheEnnemi(int(messageSplit[1]), int(messageSplit[2]))
                elif messageSplit[0] == "Rate":
                    self.jeu.ajouterRateEnnemi(int(messageSplit[1]), int(messageSplit[2]))
                elif messageSplit[0] == "Gagne":
                    self.jeu.gagne = True
                    self.continuer = False
                elif messageSplit[0] == "Perdu":
                    self.jeu.perdu = True
                    self.continuer = False
                elif messageSplit[0] == "Tour":
                    self.jeu.tour = True
                elif messageSplit[0] == "Tir":
                    self.jeu.verifierTir(int(messageSplit[1]), int(messageSplit[2]))
                elif messageSplit[0] == "Bateau":
                    self.jeu.ajouterBateauCoule(message)
                elif messageSplit[0] == "PartieTrouvee":
                    self.jeu.partieTrouvee = True

        print("Arret du TreadSocket")
        self.ferme = True
        self.connexionServeur.close()

    def envoyerBateau(self, str):
        self.connexionServeur.send(str.encode())


    def envoyerTir(self, positionX, positionY):
        self.connexionServeur.send("Tir,{},{}.".format(int(positionX), int(positionY)).encode())

    def envoyerPret(self):
        self.connexionServeur.send("Pret.".encode())

    def stop(self):
        self.connexionServeur.send("fin.".encode())
        while not self.ferme:
            continue


