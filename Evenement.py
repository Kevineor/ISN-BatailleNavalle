﻿import pygame
from pygame.locals import *

class Evenement:
    def __init__(self):
        #initialisation des variables de nos evenements (si besoin)
        self.clicGauche = False
        self.clicDroit = False
        self.positionX = 0
        self.positionY = 0
        self.recomencer = False
        self.entrer = False

    def update(self):
        #on set une variable continuer pour eviter la multiplication des return
        continuer = True
        for event in pygame.event.get():
            if event.type == QUIT:
                continuer = False
            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    continuer = False
                if event.key == K_r:
                    self.recomencer = True
                elif event.key == K_RETURN:
                    self.entrer = True
            elif event.type == KEYUP:
                if event.key == K_r:
                    self.recomencer = False
                elif event.key == K_RETURN:
                    self.entrer = False
            elif event.type == MOUSEBUTTONDOWN:
                if event.button == 1:
                    self.clicGauche = True
                    self.positionX = event.pos[0]
                    self.positionY = event.pos[1]
                if event.button == 3:
                    self.clicDroit = True
                    self.positionX = event.pos[0]
                    self.positionY = event.pos[1]
            elif event.type == MOUSEBUTTONUP:
                if event.button == 1 and self.clicGauche:
                    self.clicGauche = False
                if event.button == 3 and self.clicDroit:
                    self.clicDroit = False
            elif event.type == MOUSEMOTION:
                self.positionX = event.pos[0]
                self.positionY = event.pos[1]
        return continuer