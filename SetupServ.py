from cx_Freeze import setup, Executable

options = {
    'build_exe': {
        'include_msvcr': True,
    }
}

setup (
    name = "BattleChips Serveur",
    version = "1",
    description = "Serveur de jeu bataille navale ISN ANDRIEUX DEGIOANNI MASSON",
    options = options,
    executables = [Executable("Serveur.py")],
)