import pygame
from Evenement import *
from ObjetAffichable import*
from Bouton import *


class Menu():
    def __init__(self):
        self.ecran = pygame.Surface((992,448))
        self.fond1 = pygame.image.load("assets/Menu/FondMenu.jpg")
        self.event = Evenement()

        self.boutonP = Bouton(13, 10, 5, 2, "assets/Menu/play.png")

        self.BVolumeOn = Bouton(29, 12, 1, 1, "assets/Menu/volumeOn.png")
        self.BVolumeOff = Bouton(29, 12, 1, 1, "assets/Menu/volumeOff.png")
        self.BVolume = self.BVolumeOn
        self.BReseau = Bouton(14, 5, 3, 1, "assets/Menu/Reseau.png")
        self.BSolo = Bouton(14, 5, 3, 1, "assets/Menu/Solo.png")
        self.BLigne = self.BReseau
        self.ligne = True

        self.volume = True



        self.EnLigne = True #Variable permettant de définir si la partie se joue en ligne

    def update_event(self):
        continuer = self.event.update()
        jouer = False
        self.boutonP.verifSurlignage(self.event)

        if self.boutonP.pointe(self.event) and self.event.clicGauche:
            jouer = True
            self.event.clicGauche = False

        self.BVolume.verifSurlignage(self.event)

        if self.BVolume.pointe(self.event) and self.event.clicGauche:
            if self.volume == True:
                self.BVolume = self.BVolumeOff
                self.volume = False
                self.event.clicGauche = False

            else:
                self.volume = True
                self.BVolume = self.BVolumeOn
                self.event.clicGauche = False


        self.BLigne.verifSurlignage(self.event)

        if self.BLigne.pointe(self.event) and self.event.clicGauche:
            if self.ligne == True:
                self.BLigne = self.BSolo
                self.ligne = False
                self.event.clicGauche = False

            else:
                self.ligne = True
                self.BLigne = self.BReseau
                self.event.clicGauche = False


        return (continuer, jouer)

    def update(self):
        self.ecran.blit(self.fond1, (0, 0))
        self.boutonP.afficher(self.ecran)
        self.BVolume.afficher(self.ecran)
        self.BLigne.afficher(self.ecran)

