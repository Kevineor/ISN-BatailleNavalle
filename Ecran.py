import pygame



class Ecran():


    def __init__(self, listeObjets):
        self.Surface = pygame.Surface((10 * 32, 10 * 32), pygame.SRCALPHA)
        self.listeObjets = listeObjets
        self.update(self.listeObjets)



    def update(self, listeObjets):
        self.Surface.fill((0, 0, 0, 0))
        for objet in listeObjets:
            objet.afficher(self.Surface)



