import pygame
from Evenement import*
from ObjetAffichable import*
from Bouton import *
from Menu import *
from Jeu import *

pygame.init()
fenetre = pygame.display.set_mode((992,448))
event = Evenement()
continuer = True
menu = Menu()
clock = pygame.time.Clock()
jouer = False
x = 0
y = 0
while continuer:
    while continuer and not jouer:
        clock.tick(60)
        fenetre.fill((0, 0, 0))
        continuer, jouer = menu.update_event()
        menu.update()
        fenetre.blit(menu.ecran, (0,0))
        pygame.display.flip()
    if jouer:
        jeu = Jeu(menu.ligne, menu.volume)
        while continuer and (not jeu.perdu) and (not jeu.gagne) and (not jeu.enemi.ferme):
            clock.tick(100)
            continuer = jeu.update_event()
            jeu.update()
            fenetre.blit(jeu.ecran, (0, 0))
            pygame.display.flip()
        jeu.stop()
        while not jeu.enemi.ferme:
            continue
        jouer = False

pygame.quit()